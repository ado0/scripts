#!/usr/bin/env bash

pid=$(swaymsg -t get_tree | jq '.. | select(.type?) | select(.type=="con") | select(.focused==true) | select(.app_id=="Alacritty").pid')
ppid=$(pgrep --newest --parent ${pid})
alacritty --working-directory "$(readlink /proc/${ppid}/cwd || echo $HOME)" || alacritty
