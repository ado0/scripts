#! /usr/bin/env sh
#
# Search torrents and stream them using peerflix and mpv.

TMP_FOLDER="$HOME/temp"

pirate-get -C "peerflix %s --mpv -f $TMP_FOLDER" "$@"
