#!/usr/bin/env bash

if pgrep -x "slurp" > /dev/null
then
	exit 0
fi

GEOM="$(slurp -d)"
grim -g "$GEOM" - | wl-copy

DST_DIR=$(date +"grim_%Y_%b");
DST_FILE=$(date +"%Y-%m-%d_%H-%M-%S_grim_screenshot.png");
mkdir -p ~/.scrot/"${DST_DIR}"
wl-paste > ~/.scrot/"$DST_DIR"/"$DST_FILE"
